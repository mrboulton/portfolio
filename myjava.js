
$(document).ready(function(){

// just to get it out the way, i know my java is basic
// didn't have time to create functions due to personal reasons
// It will be completed... at some point

// allowing user to hover of emoji project
  $("#emojis").hover(function(){
    $("#colouredimg").hide();
    $("#headingtext").hide();
    $("#calltoaction").show();
    $("#sketchimg").show();
  }, function(){
    $("#colouredimg").show();
    $("#sketchimg").hide();
    $("#headingtext").show();
    $("#calltoaction").hide();
  });

// allowing user to hover of skyparksecure project
  $("#skyparksecure").hover(function(){
    $("#dissertation").hide();
    $("#skyPimg").hide();
    $("#SkyParktext").show();
    document.getElementById("skyparksecure").style.width = "600px";
    $("#Skybutton").show();
  }, function(){
    $("#dissertation").show();
    document.getElementById("skyparksecure").style.width = "100%";
    $("#skyPimg").show();
    $("#SkyParktext").hide();
    $("#Skybutton").hide();
  });

// allowing users to jump between content on indivisual pages
  $("#initialD").click(function(){
    $("#early").show();
    $("#end").hide();
    $("#middle").hide();
    document.getElementById("initialD").style.backgroundColor = "#1D6A96";
    document.getElementById("screen").style.backgroundColor = "#D1DDDB";
    document.getElementById("product").style.backgroundColor = "#D1DDDB";
  });

  $("#screen").click(function(){
    $("#early").hide();
    $("#end").hide();
    $("#middle").show();
    document.getElementById("initialD").style.backgroundColor = "#D1DDDB";
    document.getElementById("screen").style.backgroundColor = "#1D6A96";
    document.getElementById("product").style.backgroundColor = "#D1DDDB";
  });

  $("#product").click(function(){
    $("#early").hide();
    $("#end").show();
    $("#middle").hide();
    document.getElementById("initialD").style.backgroundColor = "#D1DDDB";
    document.getElementById("screen").style.backgroundColor = "#D1DDDB";
    document.getElementById("product").style.backgroundColor = "#1D6A96";
  });

  // allowing user to hover of ruby project

  $('#ruby').hover(function(){
    this.style.backgroundColor = "#1a1a27";
    $('#rubyAction').show();
      }, function(){
    this.style.backgroundColor = "#85B8CB";
    $('#rubyAction').hide();
  });




});
